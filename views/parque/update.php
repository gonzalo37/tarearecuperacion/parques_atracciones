<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Parques */

$this->title = 'Update Parques: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Parques', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="parques-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
