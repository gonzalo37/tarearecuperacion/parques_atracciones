<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pertenece */

$this->title = 'Create Pertenece';
$this->params['breadcrumbs'][] = ['label' => 'Perteneces', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pertenece-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
