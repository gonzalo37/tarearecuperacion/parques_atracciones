<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dirigentes */

$this->title = 'Create Dirigentes';
$this->params['breadcrumbs'][] = ['label' => 'Dirigentes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dirigentes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
