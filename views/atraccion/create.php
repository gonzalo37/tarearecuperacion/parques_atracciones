<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Atracciones */

$this->title = 'Create Atracciones';
$this->params['breadcrumbs'][] = ['label' => 'Atracciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="atracciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
