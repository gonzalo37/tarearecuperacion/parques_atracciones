<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de los parques de atracciones';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de los parques</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar los lugares de los parques (sin repetidos)</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
            <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 2</h3>
                        <p>Número de dirigentes que hay</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
            <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 3</h3>
                        <p>Media de empleados que hay por parque</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 4</h3>
                        <p>El parque con más empleados</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
              <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 5</h3>
                        <p>Mostrar el codigo y el nombre de los empleados</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                  
                  <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 6</h3>
                        <p>Mostrar aquellos empleados que trabajen en establecimientos que empiecen por P</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                      
                      <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 7</h3>
                        <p>Mostrar los establecimientos de los parques cuyo nombre sea mayor de 10 caracteres</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>
                          
                         <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 8</h3>
                        <p>Mostrar los empleados que trabajen en un parque cuya atracción principal sea el Dragon Khan</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>   
                             
                           <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 9</h3>
                        <p>Mostrar los parques que pertenezcan a la empresa DISNEY</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div>                
                
                      <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class ="caption">
                        <h3>Consulta 10</h3>
                        <p>Mostrar el parque dirige Mark Smith</p>
                        <p>
                            <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default'])?>
                        </p>
                    </div>
                </div>  
            </div> 
                   
                   
                        
        </div>
    </div>
</div>
