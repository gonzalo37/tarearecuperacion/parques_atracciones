<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\SqlDataProvider;
use yii\data\ActiveDataProvider;
use app\models\Parques;
use app\models\Dirigentes;
use app\models\Establecimientos;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct lugar) from parques")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT lugar FROM parques",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['lugar'],
            "titulo"=>"Consulta 1 con DAO",
            "enunciado"=>"Listar los lugares de los parques (sin repetidos)",
            "sql"=>"SELECT DISTINCT lugar FROM parques",
        ]);
    }
    
    
     public function actionConsulta2(){
        $numero = Yii::$app->db
                ->createCommand("select count(distinct nombre) as contarnombre from dirigentes")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT COUNT(DISTINCT nombre) AS contarnombre FROM dirigentes",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['contarnombre'],
            "titulo"=>"Consulta 2 con DAO",
            "enunciado"=>"Número de dirigentes que hay",
            "sql"=>"SELECT COUNT(DISTINCT nombre) AS contarnombre FROM dirigentes",
        ]);
    }
    
    public function actionConsulta3(){
        $numero = Yii::$app->db
                ->createCommand("select avg(numero_empleados) as mediaempleados from parques")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT AVG(numero_empleados) AS mediaempleados FROM parques",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['mediaempleados'],
            "titulo"=>"Consulta 3 con DAO",
            "enunciado"=>"Media de empleados que hay por parque",
            "sql"=>"SELECT AVG(numero_empleados) AS mediaempleados FROM parques",
        ]);
    }
    
    public function actionConsulta4(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT nombre,MAX(numero_empleados) as maximoempleados FROM parques",
           
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre','maximoempleados'],
            "titulo"=>"Consulta 4 con DAO",
            "enunciado"=>"El parque con más empleados",
            "sql"=>"SELECT nombre,MAX(numero_empleados) as maximoempleados FROM parques",
        ]);
    }
    
     public function actionConsulta5(){
        $numero = Yii::$app->db
                ->createCommand("select codigo,nombre from empleados group by codigo")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT codigo,nombre FROM empleados GROUP BY codigo",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['codigo','nombre'],
            "titulo"=>"Consulta 5 con DAO",
            "enunciado"=>"Mostrar el codigo y el nombre de los empleados",
            "sql"=>"SELECT codigo,nombre FROM empleados GROUP BY codigo",
        ]);
    }
    
    public function actionConsulta6(){
        $numero = Yii::$app->db
                ->createCommand("select codigo_establecimiento,nombre,apellido from empleados group by codigo_establecimiento having nombre like 'P%'")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT  codigo_establecimiento,nombre,apellido FROM empleados GROUP BY codigo_establecimiento HAVING nombre LIKE 'P%'",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['codigo_establecimiento','nombre','apellido'],
            "titulo"=>"Consulta 6 con DAO",
            "enunciado"=>"Mostrar aquellos empleados que trabajen en establecimientos que empiecen por P",
            "sql"=>"SELECT  codigo_establecimiento,nombre,apellido FROM empleados GROUP BY codigo_establecimiento HAVING nombre LIKE 'P%'",
        ]);
    }
    
    public function actionConsulta7(){
        $numero = Yii::$app->db
                ->createCommand("select codigo,nombre,codigo_parque from establecimientos group by codigo having char_length(nombre)>10")
                ->queryScalar();
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT codigo,nombre,codigo_parque FROM establecimientos GROUP BY codigo HAVING CHAR_LENGTH(nombre)>10",
            'totalCount'=>$numero,
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['codigo','nombre','codigo_parque'],
            "titulo"=>"Consulta 7 con DAO",
            "enunciado"=>"Mostrar los establecimientos de los parques cuyo nombre sea mayor de 10 caracteres",
            "sql"=>"SELECT  codigo,nombre,codigo_parque FROM establecimientos GROUP BY codigo HAVING CHAR_LENGTH(nombre)>10",
        ]);
    }
    
    public function actionConsulta8(){
        
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT empleados.nombre FROM empleados INNER JOIN atracciones a ON empleados.codigo_atraccion = a.codigo WHERE a.nombre='Dragon Khan'",
            
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 8 con DAO",
            "enunciado"=>"Mostrar los empleados que trabajen en un parque cuya atracción principal sea el Dragon Khan",
            "sql"=>"SELECT DISTINCT empleados.nombre FROM empleados INNER JOIN atracciones a ON empleados.codigo_atraccion = a.codigo WHERE a.nombre='Dragon Khan'",
        ]);
    }
    
    
    public function actionConsulta9(){
        
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT parques.nombre FROM parques INNER JOIN pertenece p ON parques.codigo = p.codigo_parque INNER JOIN empresas e ON p.cif = e.cif WHERE e.nombre='DISNEY'",
            
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 9 con DAO",
            "enunciado"=>"Mostrar los parques que pertenezcan a la empresa DISNEY",
            "sql"=>"SELECT DISTINCT parques.nombre FROM parques INNER JOIN pertenece p ON parques.codigo = p.codigo_parque INNER JOIN compañias c ON p.cif = c.cif WHERE c.nombre='DISNEY'",
        ]);
    }
    
    public function actionConsulta10(){
       
        $dataprovider=new SqlDataProvider([
            'sql'=>"SELECT DISTINCT parques.nombre FROM parques INNER JOIN dirigentes USING (codigo) WHERE dirigentes.nombre='Mark' AND apellido='Smith'",
            
            'pagination'=>[
                'pageSize' => 5,
            ]
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataprovider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 10 con DAO",
            "enunciado"=>"Mostrar el parque dirige Mark Smith",
            "sql"=>"SELECT DISTINCT parques.nombre FROM parques INNER JOIN dirigentes USING (codigo) WHERE dirigentes.nombre='Mark' AND apellido='Smith'",
        ]);
    }
    
    
    
    
}
