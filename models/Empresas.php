<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas".
 *
 * @property string $cif
 * @property string|null $nombre
 *
 * @property Pertenece[] $perteneces
 * @property Parques[] $codigoParques
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cif'], 'required'],
            [['cif'], 'string', 'max' => 15],
            [['nombre'], 'string', 'max' => 20],
            [['cif'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cif' => 'Cif',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Perteneces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['cif' => 'cif']);
    }

    /**
     * Gets query for [[CodigoParques]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoParques()
    {
        return $this->hasMany(Parques::className(), ['codigo' => 'codigo_parque'])->viaTable('pertenece', ['cif' => 'cif']);
    }
}
