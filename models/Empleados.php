<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $apellido
 * @property int|null $codigo_establecimiento
 * @property int|null $codigo_atraccion
 *
 * @property Atracciones $codigoAtraccion
 * @property Establecimientos $codigoEstablecimiento
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_establecimiento', 'codigo_atraccion'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['apellido'], 'string', 'max' => 30],
            [['codigo'], 'unique'],
            [['codigo_atraccion'], 'exist', 'skipOnError' => true, 'targetClass' => Atracciones::className(), 'targetAttribute' => ['codigo_atraccion' => 'codigo']],
            [['codigo_establecimiento'], 'exist', 'skipOnError' => true, 'targetClass' => Establecimientos::className(), 'targetAttribute' => ['codigo_establecimiento' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'codigo_establecimiento' => 'Codigo Establecimiento',
            'codigo_atraccion' => 'Codigo Atraccion',
        ];
    }

    /**
     * Gets query for [[CodigoAtraccion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoAtraccion()
    {
        return $this->hasOne(Atracciones::className(), ['codigo' => 'codigo_atraccion']);
    }

    /**
     * Gets query for [[CodigoEstablecimiento]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoEstablecimiento()
    {
        return $this->hasOne(Establecimientos::className(), ['codigo' => 'codigo_establecimiento']);
    }
}
