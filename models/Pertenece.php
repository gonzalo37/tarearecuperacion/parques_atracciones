<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pertenece".
 *
 * @property int $id
 * @property string|null $cif
 * @property int|null $codigo_parque
 *
 * @property Empresas $cif0
 * @property Parques $codigoParque
 */
class Pertenece extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pertenece';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'codigo_parque'], 'integer'],
            [['cif'], 'string', 'max' => 15],
            [['cif', 'codigo_parque'], 'unique', 'targetAttribute' => ['cif', 'codigo_parque']],
            [['id'], 'unique'],
            [['cif'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::className(), 'targetAttribute' => ['cif' => 'cif']],
            [['codigo_parque'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['codigo_parque' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cif' => 'Cif',
            'codigo_parque' => 'Codigo Parque',
        ];
    }

    /**
     * Gets query for [[Cif0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCif0()
    {
        return $this->hasOne(Empresas::className(), ['cif' => 'cif']);
    }

    /**
     * Gets query for [[CodigoParque]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoParque()
    {
        return $this->hasOne(Parques::className(), ['codigo' => 'codigo_parque']);
    }
}
