<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "establecimientos".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property int|null $codigo_parque
 *
 * @property Empleados[] $empleados
 * @property Parques $codigoParque
 */
class Establecimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'establecimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_parque'], 'integer'],
            [['nombre'], 'string', 'max' => 50],
            [['codigo'], 'unique'],
            [['codigo_parque'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['codigo_parque' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'codigo_parque' => 'Codigo Parque',
        ];
    }

    /**
     * Gets query for [[Empleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_establecimiento' => 'codigo']);
    }

    /**
     * Gets query for [[CodigoParque]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoParque()
    {
        return $this->hasOne(Parques::className(), ['codigo' => 'codigo_parque']);
    }
}
