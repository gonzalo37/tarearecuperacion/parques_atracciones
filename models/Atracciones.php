<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "atracciones".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property int|null $codigo_parque
 *
 * @property Parques $codigoParque
 * @property Empleados[] $empleados
 */
class Atracciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'atracciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_parque'], 'integer'],
            [['nombre'], 'string', 'max' => 20],
            [['codigo'], 'unique'],
            [['codigo_parque'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['codigo_parque' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'codigo_parque' => 'Codigo Parque',
        ];
    }

    /**
     * Gets query for [[CodigoParque]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoParque()
    {
        return $this->hasOne(Parques::className(), ['codigo' => 'codigo_parque']);
    }

    /**
     * Gets query for [[Empleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpleados()
    {
        return $this->hasMany(Empleados::className(), ['codigo_atraccion' => 'codigo']);
    }
}
