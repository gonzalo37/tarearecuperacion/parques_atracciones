<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parques".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $lugar
 * @property int|null $codigo_compañia
 * @property int|null $numero_empleados
 *
 * @property Atracciones[] $atracciones
 * @property Dirigentes $dirigentes
 * @property Establecimientos[] $establecimientos
 * @property Pertenece[] $perteneces
 * @property Empresas[] $cifs
 */
class Parques extends \yii\db\ActiveRecord
{
    public $mediaempleados;
    public $maximoempleados;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parques';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_compañia', 'numero_empleados'], 'integer'],
            [['nombre', 'lugar'], 'string', 'max' => 50],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'lugar' => 'Lugar',
            'codigo_compañia' => 'Codigo Compañia',
            'numero_empleados' => 'Numero Empleados',
        ];
    }

    /**
     * Gets query for [[Atracciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAtracciones()
    {
        return $this->hasMany(Atracciones::className(), ['codigo_parque' => 'codigo']);
    }

    /**
     * Gets query for [[Dirigentes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirigentes()
    {
        return $this->hasOne(Dirigentes::className(), ['codigo_parque' => 'codigo']);
    }

    /**
     * Gets query for [[Establecimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstablecimientos()
    {
        return $this->hasMany(Establecimientos::className(), ['codigo_parque' => 'codigo']);
    }

    /**
     * Gets query for [[Perteneces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPerteneces()
    {
        return $this->hasMany(Pertenece::className(), ['codigo_parque' => 'codigo']);
    }

    /**
     * Gets query for [[Cifs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCifs()
    {
        return $this->hasMany(Empresas::className(), ['cif' => 'cif'])->viaTable('pertenece', ['codigo_parque' => 'codigo']);
    }
}
