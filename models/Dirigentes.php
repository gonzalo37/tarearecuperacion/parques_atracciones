<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dirigentes".
 *
 * @property int $codigo
 * @property string|null $nombre
 * @property string|null $apellido
 * @property int|null $codigo_parque
 *
 * @property Parques $codigoParque
 */
class Dirigentes extends \yii\db\ActiveRecord
{
    public $contarnombre;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dirigentes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_parque'], 'integer'],
            [['nombre', 'apellido'], 'string', 'max' => 20],
            [['codigo_parque'], 'unique'],
            [['codigo'], 'unique'],
            [['codigo_parque'], 'exist', 'skipOnError' => true, 'targetClass' => Parques::className(), 'targetAttribute' => ['codigo_parque' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'apellido' => 'Apellido',
            'codigo_parque' => 'Codigo Parque',
        ];
    }

    /**
     * Gets query for [[CodigoParque]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoParque()
    {
        return $this->hasOne(Parques::className(), ['codigo' => 'codigo_parque']);
    }
}
